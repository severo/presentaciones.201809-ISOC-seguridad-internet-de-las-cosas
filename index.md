---
layout: presentacion
title: Seguridad del Internet de las cosas - RFC 8240
slug: 2018-09-22-seguridad-iot
description:
  Presentación de Sylvain Lesage, taller ISOC Bolivia sobre seguridad del
  Internet de las cosas. Presentación del RFC 8240 y de los comentarios de
  Stéphane Bortzmeyer.
theme: white
transition: slide
date: 2018-09-21 08:00:00 -0400
---

<section data-markdown>
## RFC 8240 - Seguridad del Internet de las cosas

Sylvain LESAGE

---

Taller "Buenas prácticas de seguridad y privacidad para IoT en Bolivia" ISOC
Bolivia

La Paz - 22/09/2018

</section>

<section data-markdown>
## Sylvain Lesage

Activista de las tecnologías libres

- MAIL: severo@rednegra.net
- TEL: 71542024
- TWITTER: @severo_bo

</section>

<section data-markdown>
## IETF

- No soy experto
- Solo repito lo que dice la IETF -
  [RFC 8240](www.rfc-editor.org/rfc/rfc8240.txt)
- (y [@bortzmeyer](https://twitter.com/bortzmeyer) en su
  [blog](http://www.bortzmeyer.org/8240.html))

</section>

<section data-markdown>
## IETF

El [RFC 8240](www.rfc-editor.org/rfc/rfc8240.txt)

- fue escrito por H. Tschofenig (ARM Limited), S. Farrell (Trinity College
  Dublin),
- publicado en septiembre de 2017,
- se llama: "Report from the Internet of Things (IoT) Software Update (IoTSU)
  Workshop 2016"
- sintetiza los trabajos del taller de Dublin en junio de 2016.

</section>

<section data-markdown>
## Internet de las cosas

- objetos conectados = computadoras, entonces
- tienen los mismos problemas de seguridad que las computadoras

**pero**

- no son considerados computadoras por sus dueños, entonces
- nadie los administra

</section>

<section data-markdown>
## Problema principal

> Cómo actualizar los objetos

- por lo menos: tapar las fallas de seguridad
- problema: software ya desactualizado cuando el objeto sale de fábrica

</section>

<section data-markdown>
## Problema conocido...

- [Schneier, 2014](https://www.schneier.com/essays/archives/2014/01/the_internet_of_thin.html)
- [Grupo Artículo 29, 2014](http://ec.europa.eu/justice/data-protection/article-29/documentation/opinion-recommendation/files/2014/wp223_en.pdf)
- [Informe de la FTC, 2015](https://www.ftc.gov/news-events/press-releases/2015/01/ftc-report-internet-things-urges-companies-adopt-best-practices)

</section>

<section data-markdown>
## ...pero difícil de resolver

- un mecanismo de actualización (con http por ejemplo) puede facilitar los
  ataques
- las actualizaciones pueden ser malevosas e incluir backdoors
- vida privada en caso de actualización automática
- ningun incentivo comercial para que las empresas gasten en seguridad (¡hola
  regulación!)
- ¿qué hacer cuando la empresa fabricadora del objeto desaparece? (puede ser
  útil un tercer de confianza que conserve el código)
  </section>

<section data-markdown>
## Clases de objetos

Las soluciones dependen del tipo de objeto:

- televisiones, raspberry pi: potentes, alimentación externa, sistema operativo,
  paquetes
- chiquitos: batería, memoria y CPU limitados, firmware - una solución:
  [Contiki](https://es.wikipedia.org/wiki/Contiki)

y de la sensibilidad:

- _hitless_: no puedes reiniciar un auto para instalar una actualización, una
  aspiradora sí
  </section>

<section data-markdown>
## ¿Quién es responsable de la seguridad?

Para teléfonos corporativos:

- Proyecto Android
- Google
- Fabricante del teléfono
- Operador de telecomunicaciones
- Empresa
- Usuario
  </section>

<section data-markdown>
## Seguridad de las actualizaciones

- No puede ser en HTTP -> criptografía:
  - costoso para aparatos chiquitos, más que todo si es criptografía asimétrica
  - problema de revocación, evolución de criptanálisis - dificultad de
    actualizar la clave pública (peor si hay código de varias fuentes)
  - la criptografía necesita un reloj funcional - es difícil en objetos
    conectados, requiere baterías potentes y caras
    </section>

<section data-markdown>
## Otras preguntas de actualización

- _push_ o _pull_:
  - _push_ imposible detrás de cortafuegos, o si el aparato está apagado o no
    disponible
  - _pull_ gasta energía en vano
- protegerse de fallas en la actualización cuesta caro: requiere conservar dos o
  tres versiones de la actualización, y mecanismo de reversión
  </section>

<section data-markdown>
## Otros problemas

- ¿bloquear un objeto después de un tiempo sin actualización? ¿Alertar? Bueno
  del punto de vista de la seguridad, pero obsolesencia programada. ¿Que pasará
  con los objetos "huerfanos"?
- los constructores no pueden hacer seguimiento de sus objetos - problemas de
  privacidad
  </section>

<section data-markdown>
## Derechos de los usuarios

¿Rechazar actualizaciones?

- Caso del
  [iPhone de San Bernardino](https://en.wikipedia.org/wiki/2015%20San%20Bernardino%20%20%20%20%20attack#Phone_decryption)
  donde propusieron actualizarlo con una puerta trasera
- Impresoras HP que empiezan a
  [prohibir recargas de otros fabricantes](http://boingboing.net/2016/09/19/hp-detonates-its-timebomb-pri.html)
- Tesla que
  [cambia la autonomía de los autos](https://electrek.co/2017/09/09/tesla-extends-range-vehicles-for-free-in-florida-escape-hurricane-irma/)
- Amazon
  [borra libros](http://www.nytimes.com/2009/07/18/technology/companies/18amazon.html)
  en los libros electrónicos
- en caso de objetos en hospitales: no se puede actualizar así de simple, sin
  pruebas
  </section>

<section data-markdown>
## Consensos IETF

Las propiedades importantes para seguridad:

- software firmado, y verificación de la firma por el objeto
- actualizaciones parciales (no del firmware)
- posibilidad de infraestructura local / interna de actualización
  </section>

<section data-markdown>
## Propuestas del RFC

- elaborar un estándar de distribución y firma del software de actualización
- se requieren mecanismos de "transmisión del poder" entre empresas
  </section>

<section data-markdown>
## Más referencias

Para ver todos los vínculos, textos, referencias:

- [RFC 8240](https://www.rfc-editor.org/rfc/rfc8240.txt)
- [artículo de Stéphane Bortzmeyer](http://www.bortzmeyer.org/8240.html)
  </section>

<section data-markdown>
## Sylvain Lesage

Activista de las tecnologías libres

- MAIL: severo@rednegra.net
- TEL: 71542024
- TWITTER: @severo_bo

</section>
