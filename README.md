# Instalar

```
git clone --recursive git@framagit.org:severo/presentaciones.201809-UASB-inauguracion-maestria.git
```

Si solo has descargado con `git clone` sin la opción `--recursive`, tienes que
descargar los submodulos también (reveal.js):

```
git submodule update --init --recursive
```
